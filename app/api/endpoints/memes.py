from fastapi import APIRouter, Depends, HTTPException, UploadFile, File, status
from sqlalchemy.orm import Session
from typing import List
from app.db.session import SessionLocal
from app.db.models.meme import Meme
from app.schemas.meme import MemeCreate, MemeUpdate, Meme as MemeSchema
from app.services.s3 import upload_image_to_s3

router = APIRouter()

def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

@router.get("/memes", response_model=List[MemeSchema])
def get_memes(skip: int = 0, limit: int = 10, db: Session = Depends(get_db)):
    memes = db.query(Meme).offset(skip).limit(limit).all()
    return memes

@router.get("/memes/{id}", response_model=MemeSchema)
def get_meme(id: int, db: Session = Depends(get_db)):
    meme = db.query(Meme).filter(Meme.id == id).first()
    if not meme:
        raise HTTPException(status_code=404, detail="Meme not found")
    return meme

@router.post("/memes", response_model=MemeSchema)
def create_meme(meme: MemeCreate = Depends(), image: UploadFile = File(...), db: Session = Depends(get_db)):
    try:
        print(f"Received meme: {meme}")
        print(f"Received image: {image.filename}")
        image_url = upload_image_to_s3(image)
        print(f"Uploaded image URL: {image_url}")
        db_meme = Meme(title=meme.title, description=meme.description, image_url=image_url)
        db.add(db_meme)
        db.commit()
        db.refresh(db_meme)
        return db_meme
    except Exception as e:
        print(f"Error: {str(e)}")
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail=str(e))

@router.put("/memes/{id}", response_model=MemeSchema)
def update_meme(id: int, meme: MemeUpdate, db: Session = Depends(get_db)):
    db_meme = db.query(Meme).filter(Meme.id == id).first()
    if not db_meme:
        raise HTTPException(status_code=404, detail="Meme not found")
    db_meme.title = meme.title
    db_meme.description = meme.description
    db.commit()
    db.refresh(db_meme)
    return db_meme

@router.delete("/memes/{id}", response_model=MemeSchema)
def delete_meme(id: int, db: Session = Depends(get_db)):
    db_meme = db.query(Meme).filter(Meme.id == id).first()
    if not db_meme:
        raise HTTPException(status_code=404, detail="Meme not found")
    db.delete(db_meme)
    db.commit()
    return db_meme


