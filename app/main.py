from fastapi import FastAPI
from app.api.endpoints import memes
from app.db.session import Base, engine

app = FastAPI()

Base.metadata.create_all(bind=engine)

app.include_router(memes.router, prefix="/api/v1", tags=["memes"])

@app.get("/")
def read_root():
    return {"message": "Welcome to the Meme API. Documentation is available at /docs"}

