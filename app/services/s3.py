import boto3
from fastapi import UploadFile
from app.core.config import settings
import uuid

s3_client = boto3.client(
    's3',
    endpoint_url=settings.S3_ENDPOINT_URL,
    aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
    aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY
)

def upload_image_to_s3(file: UploadFile):
    file_extension = file.filename.split(".")[-1]
    file_key = f"{uuid.uuid4()}.{file_extension}"
    s3_client.upload_fileobj(file.file, settings.S3_BUCKET_NAME, file_key)
    return f"{settings.S3_ENDPOINT_URL}/{settings.S3_BUCKET_NAME}/{file_key}"
